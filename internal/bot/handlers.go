package bot

import (
	"go-spacexbot/assets"
	tb "gopkg.in/tucnak/telebot.v2"
)

func (b TelegramBot) handlerStart(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, s.Localize("Start"))
}

func (b TelegramBot) handlerHelp(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, s.Localize("Help"))
}

func (b TelegramBot) handlerKeyboard(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, s.Localize("KeyboardOpen"), mainKeyboard(s.Localize))
}

func (b TelegramBot) handlerNext(s *Session, m *tb.Message) {
	mission, err := b.api.Next()
	if err != nil {
		s.processError(err)
		return
	}
	_, _ = b.tg.Send(m.Chat, mission.Title)
}

func (b TelegramBot) handlerUpcoming(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerUpcoming")
}

func (b TelegramBot) handlerPicture(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerPicture")
}

func (b TelegramBot) handlerSettings(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "ностройки")
}

func (b TelegramBot) handlerLast(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerLast")

}
func (b TelegramBot) handlerGet(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerGet")
}

func (b TelegramBot) handlerStream(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerStream")
}

func (b TelegramBot) handlerSubscribe(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerSubscribe")
}

func (b TelegramBot) handlerExtend(s *Session, m *tb.Message) {
	_, _ = b.tg.Send(m.Chat, "handlerExtend")
}

func mainKeyboard(l assets.Localize) *tb.ReplyMarkup {
	return &tb.ReplyMarkup{
		ReplyKeyboard: [][]tb.ReplyButton{
			{{Text: l("KeyboardSettings")}},
			{
				{Text: l("KeyboardNext")},
				{Text: l("KeyboardUpcoming")},
			},
			{{Text: l("KeyboardPicture")}},
			{{Text: l("KeyboardLastTweet")}},
			{{Text: l("KeyboardStreamLink")}},
		},
		ForceReply:          false,
		ResizeReplyKeyboard: true,
	}
}

package bot

import (
	"encoding/json"
	"go-spacexbot/internal/bot/models"
	"net/http"
	"time"
)

var SpaceXDataApiUrl = "https://api.spacexdata.com/v4/"

type launch struct {
	Fairings interface{} `json:"fairings"`
	Links    struct {
		Patch struct {
			Small string `json:"small"`
			Large string `json:"large"`
		} `json:"patch"`
		Reddit struct {
			Campaign string      `json:"campaign"`
			Launch   string      `json:"launch"`
			Media    string      `json:"media"`
			Recovery interface{} `json:"recovery"`
		} `json:"reddit"`
		Flickr struct {
			Small    []interface{} `json:"small"`
			Original []string      `json:"original"`
		} `json:"flickr"`
		Presskit  string `json:"presskit"`
		Webcast   string `json:"webcast"`
		YoutubeID string `json:"youtube_id"`
		Article   string `json:"article"`
		Wikipedia string `json:"wikipedia"`
	} `json:"links"`
	StaticFireDateUtc  time.Time     `json:"static_fire_date_utc"`
	StaticFireDateUnix int           `json:"static_fire_date_unix"`
	Tdb                bool          `json:"tdb"`
	Net                bool          `json:"net"`
	Window             int           `json:"window"`
	Rocket             string        `json:"rocket"`
	Success            bool          `json:"success"`
	Failures           []interface{} `json:"failures"`
	Details            string        `json:"details"`
	Crew               []interface{} `json:"crew"`
	Ships              []interface{} `json:"ships"`
	Capsules           []string      `json:"capsules"`
	Payloads           []string      `json:"payloads"`
	Launchpad          string        `json:"launchpad"`
	AutoUpdate         bool          `json:"auto_update"`
	FlightNumber       int           `json:"flight_number"`
	Name               string        `json:"name"`
	DateUtc            time.Time     `json:"date_utc"`
	DateUnix           int           `json:"date_unix"`
	DateLocal          string        `json:"date_local"`
	DatePrecision      string        `json:"date_precision"`
	Upcoming           bool          `json:"upcoming"`
	Cores              []struct {
		Core           string `json:"core"`
		Flight         int    `json:"flight"`
		Gridfins       bool   `json:"gridfins"`
		Legs           bool   `json:"legs"`
		Reused         bool   `json:"reused"`
		LandingAttempt bool   `json:"landing_attempt"`
		LandingSuccess bool   `json:"landing_success"`
		LandingType    string `json:"landing_type"`
		Landpad        string `json:"landpad"`
	} `json:"cores"`
	ID string `json:"id"`
}
type payload struct {
	Name            string      `json:"name"`
	Type            string      `json:"type"`
	Reused          bool        `json:"reused"`
	Launch          string      `json:"launch"`
	Customers       []string    `json:"customers"`
	Nationalities   []string    `json:"nationalities"`
	Manufacturers   []string    `json:"manufacturers"`
	MassKg          int         `json:"mass_kg"`
	MassLbs         float64     `json:"mass_lbs"`
	Orbit           string      `json:"orbit"`
	ReferenceSystem string      `json:"reference_system"`
	Regime          string      `json:"regime"`
	Longitude       interface{} `json:"longitude"`
	SemiMajorAxisKm float64     `json:"semi_major_axis_km"`
	Eccentricity    float64     `json:"eccentricity"`
	PeriapsisKm     float64     `json:"periapsis_km"`
	ApoapsisKm      float64     `json:"apoapsis_km"`
	InclinationDeg  float64     `json:"inclination_deg"`
	PeriodMin       float64     `json:"period_min"`
	LifespanYears   int         `json:"lifespan_years"`
	Epoch           time.Time   `json:"epoch"`
	MeanMotion      float64     `json:"mean_motion"`
	Raan            float64     `json:"raan"`
	ArgOfPericenter float64     `json:"arg_of_pericenter"`
	MeanAnomaly     float64     `json:"mean_anomaly"`
	ID              string      `json:"id"`
}
type rocket struct {
	Height struct {
		Meters int     `json:"meters"`
		Feet   float64 `json:"feet"`
	} `json:"height"`
	Diameter struct {
		Meters float64 `json:"meters"`
		Feet   float64 `json:"feet"`
	} `json:"diameter"`
	Mass struct {
		Kg int `json:"kg"`
		Lb int `json:"lb"`
	} `json:"mass"`
	FirstStage struct {
		ThrustSeaLevel struct {
			KN  int `json:"kN"`
			Lbf int `json:"lbf"`
		} `json:"thrust_sea_level"`
		ThrustVacuum struct {
			KN  int `json:"kN"`
			Lbf int `json:"lbf"`
		} `json:"thrust_vacuum"`
		Reusable       bool `json:"reusable"`
		Engines        int  `json:"engines"`
		FuelAmountTons int  `json:"fuel_amount_tons"`
		BurnTimeSec    int  `json:"burn_time_sec"`
	} `json:"first_stage"`
	SecondStage struct {
		Thrust struct {
			KN  int `json:"kN"`
			Lbf int `json:"lbf"`
		} `json:"thrust"`
		Payloads struct {
			CompositeFairing struct {
				Height struct {
					Meters float64 `json:"meters"`
					Feet   int     `json:"feet"`
				} `json:"height"`
				Diameter struct {
					Meters float64 `json:"meters"`
					Feet   float64 `json:"feet"`
				} `json:"diameter"`
			} `json:"composite_fairing"`
			Option1 string `json:"option_1"`
		} `json:"payloads"`
		Reusable       bool `json:"reusable"`
		Engines        int  `json:"engines"`
		FuelAmountTons int  `json:"fuel_amount_tons"`
		BurnTimeSec    int  `json:"burn_time_sec"`
	} `json:"second_stage"`
	Engines struct {
		Isp struct {
			SeaLevel int `json:"sea_level"`
			Vacuum   int `json:"vacuum"`
		} `json:"isp"`
		ThrustSeaLevel struct {
			KN  int `json:"kN"`
			Lbf int `json:"lbf"`
		} `json:"thrust_sea_level"`
		ThrustVacuum struct {
			KN  int `json:"kN"`
			Lbf int `json:"lbf"`
		} `json:"thrust_vacuum"`
		Number         int     `json:"number"`
		Type           string  `json:"type"`
		Version        string  `json:"version"`
		Layout         string  `json:"layout"`
		EngineLossMax  int     `json:"engine_loss_max"`
		Propellant1    string  `json:"propellant_1"`
		Propellant2    string  `json:"propellant_2"`
		ThrustToWeight float64 `json:"thrust_to_weight"`
	} `json:"engines"`
	LandingLegs struct {
		Number   int    `json:"number"`
		Material string `json:"material"`
	} `json:"landing_legs"`
	PayloadWeights []struct {
		ID   string `json:"id"`
		Name string `json:"name"`
		Kg   int    `json:"kg"`
		Lb   int    `json:"lb"`
	} `json:"payload_weights"`
	FlickrImages   []string `json:"flickr_images"`
	Name           string   `json:"name"`
	Type           string   `json:"type"`
	Active         bool     `json:"active"`
	Stages         int      `json:"stages"`
	Boosters       int      `json:"boosters"`
	CostPerLaunch  int      `json:"cost_per_launch"`
	SuccessRatePct int      `json:"success_rate_pct"`
	FirstFlight    string   `json:"first_flight"`
	Country        string   `json:"country"`
	Company        string   `json:"company"`
	Wikipedia      string   `json:"wikipedia"`
	Description    string   `json:"description"`
	ID             string   `json:"id"`
}

type SpaceXDataApi struct {
	rockets  map[string]*rocket
	payloads map[string]*payload
}

func NewSpaceXDataApi() *SpaceXDataApi {
	return &SpaceXDataApi{rockets: map[string]*rocket{}, payloads: map[string]*payload{}}
}

func (a SpaceXDataApi) Next() (*models.Mission, error) {
	l := launch{}
	err := method("launches/next", &l)
	if err != nil {
		return nil, err
	}
	return a.launchToMission(l)
}

func (a SpaceXDataApi) getRocket(rocketId string) (*rocket, error) {
	if rocket, ok := a.rockets[rocketId]; ok {
		return rocket, nil
	}
	r := new(rocket)
	err := method("rockets/"+rocketId, r)
	a.rockets[rocketId] = r
	return r, err
}

func (a SpaceXDataApi) getPayload(payloadId string) (*payload, error) {
	if payload, ok := a.payloads[payloadId]; ok {
		return payload, nil
	}
	p := new(payload)
	err := method("payloads/"+payloadId, p)
	a.payloads[payloadId] = p
	return p, err
}

func (a SpaceXDataApi) launchToMission(launch launch) (*models.Mission, error) {
	customer := ""
	if len(launch.Payloads) > 0 {
		payload, err := a.getPayload(launch.Payloads[0])
		if err != nil {
			return nil, err
		}
		if len(payload.Customers) > 0 {
			customer = payload.Customers[0]
		}
	}
	r, err := a.getRocket(launch.Rocket)
	if err != nil {
		return nil, err
	}
	mission := models.Mission{
		Title:      launch.Name,
		NetStatus:  launch.DatePrecision,
		LaunchTime: launch.DateUtc,
		Customer:   customer,
		Vehicle:    r.Name,
		PatchUrl:   launch.Links.Patch.Small,
		Info:       launch.Details,
	}
	return &mission, nil
}

func method(method string, out interface{}) error {
	client := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodGet, SpaceXDataApiUrl+method, nil)
	if err != nil {
		return err
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	return json.NewDecoder(res.Body).Decode(out)

}

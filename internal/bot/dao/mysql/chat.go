package mysql

import (
	"github.com/jmoiron/sqlx"
	"go-spacexbot/internal/bot/models"
)

type ChatDao struct {
	db *sqlx.DB
}

func NewChatDao(db *sqlx.DB) *ChatDao {
	return &ChatDao{db: db}
}

func (d ChatDao) ById(id int64) (*models.Chat, error) {
	row := d.db.QueryRowx("SELECT id, to_notify, language, extended FROM chats WHERE id = ? LIMIT 1", id)
	chat := new(models.Chat)
	return chat, row.StructScan(chat)
}

func (d ChatDao) AllWithNotify(notify bool) ([]models.Chat, error) {
	panic("implement me")
}

func (d ChatDao) Update(c *models.Chat) error {
	panic("implement me")
}

func (d ChatDao) Delete(id int) error {
	panic("implement me")
}

package dao

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"go-spacexbot/internal/bot/dao/mysql"
)

type Mysql struct {
	db           *sqlx.DB
	chat         *mysql.ChatDao
	subscription *mysql.SubscriptionDao
}

func NewMysql(databaseUrl string) (Dao, error) {
	db, err := sqlx.Connect("mysql", databaseUrl)
	if err != nil {
		return nil, err
	}

	dao := &Mysql{
		db:           db,
		chat:         mysql.NewChatDao(db),
		subscription: mysql.NewSubscriptionDao(db),
	}

	return dao, nil
}

func (m Mysql) Chat() ChatDao {
	return m.chat
}

func (m Mysql) Subscriptions() SubscriptionDao {
	return m.subscription
}
